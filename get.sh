#!/bin/sh

. "$PWD.conf"

while :; do
	URL="https://www.olx.ua/uk$CATEGORY/"
	[ -n "$SEARCH_TERM" ] && URL="${URL}q-$SEARCH_TERM/"
	URL="$URL?search[order]=filter_float_price%3Aasc"
	[ -n "$SEARCH_TERM" ] && URL="$URL&search[description]=1"
	[ -n "$PRICE_MIN" ] && URL="$URL&search[filter_float_price%3Afrom]=$PRICE_MIN"
	PAGE=1
	while [ "$PAGE" -le 25 ]; do
		printf '%s\n' "PAGE: $PAGE"
		wget -qO- "$URL&page=$PAGE" | tr -d '\n' |
		sed 's/<div id="div_gpt_listing_content2">.*/\n/;s/.*<h2>Звичайні//;s#</tr>\s*<tr [^>]*>#\n#g;s/[^\n]*\n//' |
		sed -r 's#.*(>Без фото<|"https://[^"]*/([^"]*)-).*"https://[^"]*-([^"]*)\..*<strong>([^<]*).*<strong>(\d*) ?(\d*).*#\5\6 \3 \2 \4#;s#/#\&sol;#g' |
		while IFS=' ' read -r PRICE ID IMG TITLE; do
			[ "$PRICE" -gt "$PRICE_MAX" ] && exit 1
			if [ -e "$PRICE $ID $IMG" ]; then
				touch "$PRICE $ID $IMG"
				continue
			fi
			rm -f *" $ID "*
			printf '%s\n' "$PRICE $ID $IMG $TITLE"
			{
				printf '%s\n' "$TITLE"
				wget -qO- "https://www.olx.ua/d/uk/obyavlenie/-$ID.html" | tr -d '\n' |
				sed -n '/<div class="css-g5mtbi-Text">/{s/.*<div class="css-g5mtbi-Text">//;s#</div>.*#\n#;s/<br[^>]*>/\n/g;p}'
			} >"$PRICE $ID $IMG"
		done || exit
		: "$((PAGE++))"
	done
	PRICE_MIN=$(ls -t | head -1 | cut -d ' ' -f 1)
done >"$PWD.log" 2>&1

