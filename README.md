# How to use
```sh
cd data/example
"$EDITOR" "$PWD.conf"  # edit configuration file
../../get.sh           # get the data from olx
../../filter.sh        # create html file with filtered items
xdg-open "$PWD.html"   # view result
```

# Cleaning
```sh
cd data/example
date
../../get.sh
../../get.sh                     # check at least twice to not miss out on anything
date
find -type f -mmin +360 -delete  # remove old files (older than 6 hours in this example)
```
