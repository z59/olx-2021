#!/bin/sh

. "$PWD.conf"

exec >"$PWD.html"
cat <<'END'
<!DOCTYPE html>
<base target="_blank">
<style>
a {
	display: block;
}
img {
	float: left;
	margin: 0 15px 8px 0;
	width: 200px;
}
hr {
	clear: both;
	margin-bottom: 0;
}
</style>
END

find . -type f -exec awk -vIGNORECASE=1 '-vRS=\1\1\1\1' "$FILTER { print FILENAME }" {} + | sed 's#\./##' | sort -t' ' -nk1 |
while IFS=' ' read -r PRICE ID IMG; do
	FNAME="$PRICE $ID $IMG"
	[ -n "$IMG" ] && IMG="<img src=\"https://ireland.apollo.olxcdn.com/v1/files/$IMG-UA/image;s=644x461\">"
	printf '%s\n' "<a href=\"https://www.olx.ua/d/uk/obyavlenie/-$ID.html\">$IMG<b>$PRICE грн</b> $(head -1 "$FNAME")</a>$(tail -n +2 "$FNAME")<hr>"
done

